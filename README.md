## Spack Legion Repo

This project was created to help the installation of the Terra and Regent.

### Installing Spack and adding the repo

1. [Install spack](https://spack.readthedocs.io/en/latest/getting_started.html)
   * `git clone https://github.com/spack/spack.git`
   * [For shell support](https://spack.readthedocs.io/en/latest/getting_started.html#shell-support) in bash: `. spack/share/spack/setup-env.sh`
2. Add this project as a spack repo:
   1. Clone this project:
      * `git clone https://gitlab.com/thiagotei/spack-legion-repo.git`
   1. Add this as spack repo:
      * `spack repo add spack-legion-repo`
   1. You can check if the repo is correctly added:
      * `spack repo list`

### Installing Regent using Spack

Check for instrunctions for specific machines [here](./README.md#instructions-for-specific-machines).
Terra is automatically installed by installing Regent.

1. Information about the packages:
   * `spack info regent`
2. Installing regent:
   * `spack install regent`
3. Before use:
   * `spack load regent`

As a result of `spack load regent`, the env var `TERRA_INSTALL_PATH` and `LEGION_DIR` are set after regent is loaded.

### Useful Spack commands

1. Listing installed packages:
   * `spack find`
2. Printing the path of the package installation:
   * `spack location -i regent`
3. Compilers recognized by Spack:
   * `spack compilers`
4. Search for installed compilers in the system:
   * `spack compiler find`
   * Obs: It may require a `module load <compiler>` for Spack to find it.
5. Load a installed package:
   * `spack load <package>`
6. Check the installation dependencies:
   * `spack spec <package>`
7. Listing loaded packages
   * `spack find --loaded`
8. Executing Step commands step by step may help for debugging. [More information here.](https://spack.readthedocs.io/en/latest/packaging_guide.html#packaging-workflow-commands)
   * `spack install -j 1 --verbose --fail-fast --keep-stage <spec>` also helps.

### Using System's packages

[Reference](https://spack.readthedocs.io/en/latest/build_settings.html?highlight=packages#external-packages)

Using pre-installed system's packages (e.g., mpi, gcc, llvm)
can save a lot of time during installation.
These packages can be recognized automatically by using the command [`spack external find`](https://spack.readthedocs.io/en/latest/command_index.html#spack-external-find). You can help Spack locate externals which use module files by loading any associated modules for packages that you want Spack to know about before running spack external find.

The packages configuration can tell Spack to use an external location for certain package versions, but it does not restrict Spack to using external packages. In the above example, since newer versions of OpenMPI are available, Spack will choose to start building and linking with the latest version rather than continue using the pre-installed OpenMPI versions.

To prevent this, the packages.yaml configuration also allows packages to be flagged as non-buildable. The `~/.spack/packages.yaml` could be modified to be:

```
packages:
  mpi:
    buildable: False
  openmpi:
    externals:
    - spec: "openmpi@1.4.3%gcc@4.4.7 arch=linux-debian7-x86_64"
      prefix: /opt/openmpi-1.4.3
    - spec: "openmpi@1.4.3%gcc@4.4.7 arch=linux-debian7-x86_64+debug"
      prefix: /opt/openmpi-1.4.3-debug
    - spec: "openmpi@1.6.5%intel@10.1 arch=linux-debian7-x86_64"
      prefix: /opt/openmpi-1.6.5-intel
```

### Instructions for specific machines

#### Lassen

1. Installing Regent using Legion's control replication branch:
   * `spack install regent@cr %gcc@7.3.1 +cuda +cuda_hijack cuda_arch=70  network=gasnet conduit=ibv +openmp +hdf5 ^cuda@10.1.243 ^terra@2021-06-04 ^llvm@6.0.1 ^spectrum-mpi`
   * Obs: it uses `gcc@7.3.1`, `spectrum-mpi`, and `cuda@10.1.243` that are already installed in the machine. `module load gcc/7.3.1 && spack compiler find && spack external find`. Change `~/.spack/packages.yaml` to `buildable: False` for these and other packages.
   * Obs 2: `cuda_arch=70` is specific for Volta GPUs.
   * Obs 3: `+cuda_hijack` was necessary to successifully compile applications with regent and cuda 10.

