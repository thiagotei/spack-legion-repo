# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *
import llnl.util.tty as tty


class Terra(CMakePackage):
    """Terra is a low-level system programming language that is embedded in and meta-programmed by the Lua programming language."""

    homepage = "https://terralang.org/"
    #TODO: change to the original repo https://github.com/terralang/terra ?
    git      = "https://github.com/StanfordLegion/terra.git"

    maintainers = ['thiagotei']

    version('master', branch='master')
    version('luajit2.1', branch='luajit2.1')
    # Includes the fix for PPC64le when LLVM does not have x86 backend
    version('2021-06-04', commit='45a8f9772f7c0342e156cbd8f0cf9bf44599b7e2')

    depends_on("llvm@3.8,5.0,6.0,7.0,8.0,9.0,10.0,11.0 +clang")
    depends_on("ncurses")
    depends_on("zlib")

    variant('lua', default='moonjit',
            values=('luajit','moonjit'),
            description='Lua implementation to use for Terra (luajit or moonjit).')

    def cmake_args(self):
        spec = self.spec
        cmake_cxx_flags = []
        options = []

        if 'lua' in spec:
            options.append("-DTERRA_LUA={}".format(spec.variants['lua'].value))

        return options

    def install(self, spec, prefix):
        super().install(spec, prefix)
        mkdir(prefix.tests)
        install_tree('tests', prefix.tests)

    def setup_run_environment(self, env):
        env.set("TERRA_INSTALL_PATH", self.prefix)

    def setup_dependent_run_environment(self, env, dependent_spec):
        env.set("TERRA_INSTALL_PATH", self.prefix)

    def setup_dependent_build_environment(self, env, dependent_spec):
        env.set("TERRA_INSTALL_PATH", self.prefix)

    # Based on https://spack.readthedocs.io/en/latest/packaging_guide.html#adding-installation-phase-tests
    # Not all tests are passing on PPC. For now commmented.
    @run_after('build')
    @on_package_attributes(run_tests=True)
    def check_build(self):
        """Run ctest in the build dir"""
        with working_dir(self.build_directory):
            ctest('-j', '4')
        
