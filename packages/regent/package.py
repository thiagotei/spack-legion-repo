# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os, json
from spack import *
from spack.pkg.builtin.legion import Legion
#from spack.build_systems.cmake import CMakePackage
import llnl.util.tty as tty

class Regent(Legion):
    """Regent is a language for implicit dataflow parallelism."""

    homepage = "https://regent-lang.org/"

    maintainers = ['thiagotei']

    version('21.03.0', tag='legion-21.03.0', submodules=True)
    version('stable', branch='stable', submodules=True)
    version('master', branch='master', submodules=True)
    version('cr', branch='control_replication', submodules=True, preferred=True)
    # These commits are used by HTR versions 1.3.0 and 1.2.3, respectivelly.
    version('e1ce6c67', commit='e1ce6c67f58a0e6b70023c31d202eebc88efa265', submodules=True)
    version('053b22f0', commit='053b22f0d590f2d23fd994e44b3a2cf45b2331c8', submodules=True)

    depends_on('terra')
    #TODO move this to the legion package.
    depends_on('hdf5 ~mpi', when='+hdf5')

    variant('shared', default=True,
            description="Build shared libraries.")

    variant('bindings', default=True,
            description="Build runtime language bindings (excl. Fortran).")

    conflicts('~shared', msg="+shared is required to build regent.")
    conflicts('~bindings', msg="+bindings is required to build regent.")

    patch('regent.3.patch', when='@21.03.0,stable,master,cr')
    patch('regent.3.legacy.patch', when='@e1ce6c67,053b22f0')

    def cmake_args(self):
        spec = self.spec
        cmake_cxx_flags = []

        options = super().cmake_args()

        options.append('-DLegion_ALLOW_MISSING_LLVM_LIBS=ON')
        options.append('-DLegion_LINK_LLVM_LIBS=OFF')

        options.append('-DLLVM_CONFIG_EXECUTABLE={}'.format(
            os.path.join(spec['llvm'].prefix.bin,'llvm-config')))

        return options

    def dump_json_config(self, filename, value):
        with open(filename, 'w') as f:
            return json.dump(value, f)

    def build(self, spec, prefix):
        # Take care of rdir https://gitlab.com/StanfordLegion/legion/-/blob/control_replication/language/install.py#L100
        with working_dir(self.stage.source_path):
            rdirmode = 'auto'
            config_filename = os.path.join(self.stage.source_path, 'language', '.rdir.json')
            tty.msg("Dumped .rdir.json in {}".format(config_filename))
            self.dump_json_config(config_filename, rdirmode)

        super().build(spec, prefix)

    def install(self, spec, prefix):
        super().install(spec, prefix)
        tty.msg("Install reference dir {}".format(os.getcwd()))
        filter_file(r'@LEGION_DIR@', "'{}'".format(prefix), 'language/regent.py', string=True)
        install('language/regent.py', prefix.bin)
        install('language/.rdir.json', prefix.bin)
        install_tree('language/src', prefix.bin.src)
        install_tree('bindings', prefix.bindings)

    def setup_run_environment(self, env):
        env.set('LEGION_DIR', self.prefix)
        #env.prepend_path('LD_LIBRARY_PATH', self.prefix.lib64)
        #env.prepend_path('INCLUDE_PATH', self.prefix.include)

#    @sean suggested to take care about the GASNET_VERSION in the legion package.
#    def setup_build_environment(self, env):
#        # This can cause problems during gasnet installation.
#        # Use gasnet_root=<path> option for using a pre-installed version.
#        env.unset('GASNET_VERSION')

    def setup_dependent_run_environment(self, env, dependent_spec):
        self.setup_run_environment(env)

    def setup_dependent_build_environment(self, env, dependent_spec):
        self.setup_run_environment(env)
