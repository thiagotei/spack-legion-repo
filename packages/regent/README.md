# Regent package

The patches tries to fix the `regent.py` script to work with Terra 
installed independently.

It requires the `TERRA_INSTALL_PATH` to be defined in the environment.
